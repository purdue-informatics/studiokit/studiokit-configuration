﻿namespace StudioKit.Configuration;

public static class BaseAppSetting
{
	public const string Tier = "TIER";

	public const string SentryDsn = "Sentry:Dsn";

	public const string WebsiteLoadCertificates = "WEBSITE_LOAD_CERTIFICATES";

	public const string SuperAdminEmailAddresses = "SuperAdminEmailAddresses";

	public const string IsDowntime = "IsDowntime";

	public const string ApplicationName = "ApplicationName";

	public const string MinWorkerThreads = "MinWorkerThreads";

	public const string MinCompletionPortThreads = "MinCompletionPortThreads";
}