namespace StudioKit.Configuration;

public interface IRelease
{
	/// <summary>
	/// The name of the application
	/// </summary>
	public string ApplicationName { get; }

	/// <summary>
	/// The version of the application
	/// </summary>
	public string Version { get; }

	/// <summary>
	/// A string representing the current version that is unique across all Studio applications (i.e. for use with Sentry) i.e. "circuit.api@2.1.1"
	/// </summary>
	public string ReleaseName { get; }

	/// <summary>
	/// The human-readable string representing the environment in which the app is running
	/// </summary>
	public string Environment { get; }
}