using System.Reflection;
using System.Text.RegularExpressions;

namespace StudioKit.Configuration;

public static class AssemblyExtensions
{
	/// <summary>
	/// Get the Version of the Assembly, but remove the last digit of the assembly's version (i.e. MS' "revision" identifier)
	/// </summary>
	/// <param name="assembly">An Assembly with a Version</param>
	public static string GetVersionWithoutRevision(this Assembly assembly)
	{
		return Regex.Replace(assembly.GetName().Version?.ToString() ?? "", @"\.\d*$", "");
	}
}