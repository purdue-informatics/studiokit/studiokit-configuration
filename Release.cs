﻿using StudioKit.Encryption;
using System.Reflection;

namespace StudioKit.Configuration;

public class Release : IRelease
{
	public Release(Assembly applicationOrWebJobAssembly)
	{
		Version = applicationOrWebJobAssembly.GetVersionWithoutRevision();
		ApplicationName = EncryptedConfigurationManager.GetSetting(BaseAppSetting.ApplicationName);
		ReleaseName = $"{ApplicationName.ToLowerInvariant()}.api@{Version}";
		Environment = Tier.FullName(EncryptedConfigurationManager.GetSetting(BaseAppSetting.Tier));
	}

	public string ApplicationName { get; }

	public string Version { get; }

	public string ReleaseName { get; }

	public string Environment { get; }
}