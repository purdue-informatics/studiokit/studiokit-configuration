﻿using System;

namespace StudioKit.Configuration;

public static class Tier
{
	/// <summary>
	/// Return a "more human" name for each tier, to allow for easier visual parsing
	/// in environments like Sentry
	/// </summary>
	/// <param name="tier">The tier</param>
	/// <returns>The "more human" name</returns>
	public static string FullName(string tier)
	{
		return tier switch
		{
			Prod => "Production",
			QA => "QA",
			Dev => "Development",
			Local => "Local",
			_ => throw new ArgumentException("Unknown tier")
		};
	}

	public const string Local = "LOCAL";

	public const string Dev = "DEV";

	public const string QA = "QA";

	public const string Prod = "PROD";
}